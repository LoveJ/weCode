import React from 'react';
import SharePopUp from "./SharePopUp.js";
import { Navbar, NavItem, Nav } from 'react-bootstrap';



export default class Header  extends React.Component {
  render() {

    let links;
    const roomURL = this.props.match.url

    if (roomURL.indexOf("/room") >= 0) {
      links = (

            <SharePopUp roomURL = {roomURL}/>
      )

    } else{
      links = (
        <NavItem eventKey={1} href="/signup">

        </NavItem>
      )
    }

    return (
      <Navbar inverse collapseOnSelect fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/"> &lt;weCode/&gt;</a>
            </Navbar.Brand>
            <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
          <Nav pullRight>
            {links}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
